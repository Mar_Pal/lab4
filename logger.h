#pragma once
#include <vector>
#include <chrono>
//z4. Klasa Logger
//Zaimplementowa� klas� Logger, b�d�c� kolekcj� kt�ra mo�e przechowywa� wiadomo�ci
//r�nych typ�w bazuj�cych na interfejsie IMessage w tym typ�w definiowanych przez u�ytkownika.Z problemem zwi�zana jest jego priorytet(np.Info, Trace, Error, itp.).
//Logger ma zapisywa� tak�e informacje o czasie wstawienia wiadomo�ci do kolekcji(wykorzysta� std::chrono),
//umo�liwia� przegl�danie zapisanych komunikat�w oraz ich wypisywanie na dowolny strumie� wyj�ciowy.Nale�y zaimplementowa� odpowiedni iterator, 
//kt�ry umo�liwiaj� np.poruszanie si� po wiadomo�ciach o okre�lonym priorytecie, czy przegl�d wiadomo�ci w okre�lonym oknie czasowym.
template <typename T>
class Logger
{	
	private:
		std::vector < char > czas;
		std::vector < T > komunikat;
		friend operator<<(std::ostream& os, const Logger<T>& l);
		friend operator>>(std::istream& is, const Logger<T>& l);
		void czas_push_back(char& cz);
		template <typename T>
		void komunikat_push_back(T & kom);
	public:

};
template <typename T>
void operator<<(std::ostream& os, const Logger<T>& l);
template <typename T>
void operator>>(std::istream& is, const Logger<T>& l);